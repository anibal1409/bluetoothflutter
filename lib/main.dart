import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

import './BluetoothDeviceListEntry.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DiscoveryPage(),
    );
  }
}
class DiscoveryPage extends StatefulWidget {
  /// If true, discovery starts on page start, otherwise user must press action button.
  final bool start;


  const DiscoveryPage({this.start = true});

  @override
  _DiscoveryPage createState() => new _DiscoveryPage();
}

class _DiscoveryPage extends State<DiscoveryPage> {
  StreamSubscription<BluetoothDiscoveryResult> _streamSubscription;
  List<BluetoothDiscoveryResult> results = List<BluetoothDiscoveryResult>();
  bool isDiscovering;
  FlutterBluetoothSerial bluetooth = FlutterBluetoothSerial.instance;
  BluetoothConnection connection;
  bool isConnecting = true;
  bool get isConnected => connection != null && connection.isConnected;
  List<_Message> messages = List<_Message>();
  String _messageBuffer = '';

  bool isDisconnecting = false;

  _DiscoveryPage();

  @override
  void initState() {
    super.initState();

    isDiscovering = widget.start;
    if (isDiscovering) {
      _startDiscovery();
    }
  }

  void _restartDiscovery() {
    setState(() {
      results.clear();
      isDiscovering = true;
    });

    _startDiscovery();
  }

  void _startDiscovery() {
    _streamSubscription = FlutterBluetoothSerial.instance.startDiscovery().listen((r) {
      setState(() { results.add(r); });
    });

    _streamSubscription.onDone(() {
      setState(() { isDiscovering = false; });
    });
  }

  // @TODO . One day there should be `_pairDevice` on long tap on something... ;)

  @override
  void dispose() {
    // Avoid memory leak (`setState` after dispose) and cancel discovery
    _streamSubscription?.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: isDiscovering ? Text('Discovering devices') : Text('Discovered devices'),
          actions: <Widget>[
            (
                isDiscovering ?
                FittedBox(child: Container(
                    margin: new EdgeInsets.all(16.0),
                    child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white))
                ))
                    :
                IconButton(
                    icon: Icon(Icons.replay),
                    onPressed: _restartDiscovery
                )
            )
          ],
        ),
        body: ListView.builder(
          itemCount: results.length,
          itemBuilder: (BuildContext context, index) {
            BluetoothDiscoveryResult result = results[index];
            return BluetoothDeviceListEntry(
                device: result.device,
                rssi: result.rssi,
                onTap: ()  async {
                  print('Estado ${await bluetooth.state}');
                  if (!result.device.isBonded) {
                    print('Dispositivo no vinculado.');
                    bluetooth.bondDeviceAtAddress(result.device.address);
                  } else {
                    print('Dispositivo vinculado.');
                  }
                  if(result.device.isConnected) {
                    print('Dispositivo: ${ result.device.name} (${ result.device.address}) CONECTADO.');
                  } else {
                    print('${result.rssi} dBm');
                    BluetoothConnection _defaultConnection;
                    _defaultConnection = await BluetoothConnection.toAddress(result.device.address);
                    /*bluetooth.isEnabled.then((isConnected) {
                      if (!isConnected) {
                        bluetooth
                            .connect(result.device)
                            .timeout(Duration(seconds: 10))
                            .catchError((error) {

                        });

                      }
                    });*/
                    //await BluetoothConnection.toAddress(result.device.address.);
                    print('Dispositivo: ${ result.device.name} (${ result.device.address}) NO CONECTADO.');
                  }


                  //print(await bluetooth.isConnected);
                  //bluetooth.openSettings();
                  //bluetooth.connectToAddress(result.device.address);
                  /*bluetooth.isConnected.then((isConnected) {
                    if (!isConnected) {
                      bluetooth
                          .connect(_device)
                          .timeout(Duration(seconds: 10))
                          .catchError((error) {
                        setState(() => _pressed = false);
                      });
                      setState(() => _pressed = true);
                    }
                  });*/
                  /*try {
                    BluetoothConnection connection = await BluetoothConnection.toAddress(result.device.address);
                    print('Connected to the device');

                    connection.input.listen(_onDataReceived).onDone(() {
                      // Example: Detect which side closed the connection
                      // There should be `isDisconnecting` flag to show are we are (locally)
                      // in middle of disconnecting process, should be set before calling
                      // `dispose`, `finish` or `close`, which all causes to disconnect.
                      // If we except the disconnection, `onDone` should be fired as result.
                      // If we didn't except this (no flag set), it means closing by remote.
                      if (isDisconnecting) {
                        print('Disconnecting locally!');
                      }
                      else {
                        print('Disconnected remotely!');
                      }
                      if (this.mounted) {
                        setState(() {});
                      }
                    });
                  }
                  catch (exception) {
                    print('Cannot connect, exception occured');
                  }*/
    /* BluetoothConnection.toAddress(result.device.address)
                  .then((_connection) {
                    print('Connected to the device');
                    connection = _connection;
                    setState(() {
                      isConnecting = false;
                      isDisconnecting = false;
                    });

                    connection.input.listen(_onDataReceived).onDone(() {
                      // Example: Detect which side closed the connection
                      // There should be `isDisconnecting` flag to show are we are (locally)
                      // in middle of disconnecting process, should be set before calling
                      // `dispose`, `finish` or `close`, which all causes to disconnect.
                      // If we except the disconnection, `onDone` should be fired as result.
                      // If we didn't except this (no flag set), it means closing by remote.
                      if (isDisconnecting) {
                        print('Disconnecting locally!');
                      }
                      else {
                        print('Disconnected remotely!');
                      }
                      if (this.mounted) {
                        setState(() {});
                      }
                    });
                  }).catchError((error) {
                    print('Cannot connect, exception occured');
                    print(error);
                  });*/
                  //Navigator.of(context).pop(result.device);
                }/*,
                onLongPress: () async {
                  try {
                    bool bonded = false;
                    if (result.device.isBonded) {
                      print('Unbonding from ${result.device.address}...');
                      await FlutterBluetoothSerial.instance.removeDeviceBondWithAddress(result.device.address);
                      print('Unbonding from ${result.device.address} has succed');
                    }
                    else {
                      print('Bonding with ${result.device.address}...');
                      bonded = await FlutterBluetoothSerial.instance.bondDeviceAtAddress(result.device.address);
                      print('Bonding with ${result.device.address} has ${bonded ? 'succed' : 'failed'}.');
                    }
                    setState(() {
                      results[results.indexOf(result)] = BluetoothDiscoveryResult(
                          device: BluetoothDevice(
                            name: result.device.name ?? '',
                            address: result.device.address,
                            type: result.device.type,
                            bondState: bonded ? BluetoothBondState.bonded : BluetoothBondState.none,
                          ),
                          rssi: result.rssi
                      );
                    });
                  }
                  catch (ex) {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text('Error occured while bonding'),
                          content: Text("${ex.toString()}"),
                          actions: <Widget>[
                            new FlatButton(
                              child: new Text("Close"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  }
                }*/
            );
          },
        )
    );
  }
  void _onDataReceived(Uint8List data) {
    // Allocate buffer for parsed data
    int backspacesCounter = 0;
    data.forEach((byte) {
      if (byte == 8 || byte == 127) {
        backspacesCounter++;
      }
    });
    Uint8List buffer = Uint8List(data.length - backspacesCounter);
    int bufferIndex = buffer.length;

    // Apply backspace control character
    backspacesCounter = 0;
    for (int i = data.length - 1; i >= 0; i--) {
      if (data[i] == 8 || data[i] == 127) {
        backspacesCounter++;
      }
      else {
        if (backspacesCounter > 0) {
          backspacesCounter--;
        }
        else {
          buffer[--bufferIndex] = data[i];
        }
      }
    }

    // Create message if there is new line character
    String dataString = String.fromCharCodes(buffer);
    int index = buffer.indexOf(13);
    if (~index != 0) { // \r\n
      setState(() {
        messages.add(_Message(1,
            backspacesCounter > 0
                ? _messageBuffer.substring(0, _messageBuffer.length - backspacesCounter)
                : _messageBuffer
                + dataString.substring(0, index)
        ));
        _messageBuffer = dataString.substring(index);
      });
    }
    else {
      _messageBuffer = (
          backspacesCounter > 0
              ? _messageBuffer.substring(0, _messageBuffer.length - backspacesCounter)
              : _messageBuffer
              + dataString
      );
    }
  }
}
class _Message {
  int whom;
  String text;

  _Message(this.whom, this.text);
}